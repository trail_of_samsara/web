let fs = require('fs');
let path = require('path');
let dust = require('dustjs-linkedin');
let links = require('./links.js');

let templates = {};

// TODO: https://expressjs.com/en/advanced/developing-template-engines.html

require.extensions['.dust'] = function (module, filename) {
	var name = path.basename(filename, '.dust');
	let tmpl = templates[name];

	if (!tmpl) {
		tmpl = templates[name] = {ts: 0};
	}

	if (Date.now() - tmpl.ts > 5 * 1000) {
		let stat = fs.statSync(`./views/${name}.dust`);
		if (stat.mtime > tmpl.ts) {
			let src = fs.readFileSync(filename, 'utf8');
			tmpl.compiled = dust.compile(src, name);
			tmpl.ts = Date.now();
			dust.loadSource(tmpl.compiled);
		}
	}

	module.exports = templates[name].compiled;
};

dust.config.whitespace = true;
dust.filters.lineBreaks = function(value) {
	return value.replace(/\n/g, '<br/>');
};

dust.onLoad = function(template, options, callback) {
	var tmpl = require(`./views/${template}.dust`);
	callback(null, tmpl);
};

async function render(template, data = {}) {
	return await new Promise(function(resolve, reject){
		dust.render(template, {common: {links}, data}, function(err, out){
			if (err) {
				reject(err);
				return;
			}

			resolve(out);
		});
	});
}

module.exports = {
	render: render,
	dust: dust
};
