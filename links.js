module.exports = [{
	url: 'https://t.me/norlin_ru',
	name: 'Telegram'
}, {
	url: 'https://discord.gg/HrQzY8Y',
	name: 'Discord'
}, {
	url: 'https://www.linkedin.com/in/norlin-ru',
	name: 'LinkedIn'
}, {
	url: 'https://twitter.com/NorlinGames',
	name: 'Twitter (en)'
}, {
	url: 'https://twitter.com/norlin_ru',
	name: 'Twitter (ru)'
}];
