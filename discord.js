const config = require('./config.js');
const discord = require('discord.js');
const client = new discord.Client();

const token = config.token;

client.on('ready', () => {
	console.log('I am ready!');
});

const noop = ()=>{};

const public_commands = ['help', 'ping', 'role'];
// TODO: get real roles list
const public_roles = ['programmers', 'artists', 'writers', 'curious'];

// remove messages in 5 sec by default
const message_timeout = 5000;

function format_options(list) {
	if (!list || !list.length)
		return '';
	return '`'+list.join('`, `')+'`';
}

class Command {
	constructor(msg) {
		this.prefix = 'public_';
		this.msg = msg;
		this.guild = msg.guild;

		let {method, params} = this.parse()||{};
		if (!method) {
			return;
		}

		this.method = method;
		this.params = params;

		this[this.prefix+this.method]();
	}

	parse() {
		let cmd = this.msg.content.split('/')[1];

		if (!cmd) {
			return;
		}

		let cmds = cmd.split(/\s/g);

		if (!cmds || !cmds.length || !public_commands.includes(cmds[0]) || !this[this.prefix+cmds[0]]) {
			return;
		}

		let method = cmds.splice(0, 1);
		let params = cmds;

		return {method, params};
	}

	findRole(name) {
		let roles = this.guild && this.guild.roles;
		if (!roles) {
			return;
		}

		name = name.toLowerCase();

		return roles.find(role=>{
			return role.name.toLowerCase() == name;
		});
	}

	send(text, timeout) {
		timeout = timeout === undefined ? message_timeout : timeout;

		this.msg.channel.send(text, {reply: this.msg.author}).then(new_msg=>{
			if (!timeout) {
				return;
			}

			setTimeout(()=>{
				this.msg.delete().catch(noop);
				new_msg.delete().catch(noop);
			}, timeout);
		}).catch(err=>{ console.log(`ERROR: can't send message!`, err); });
	}

	/* Public methods */

	public_help() {
		this.send(`List of commands: ${format_options(public_commands)}.`);
	}

	public_ping() {
		this.send('pong', 1000);
	}

	public_role() {
		let action = this.params[0];
		let member = this.msg.member;

		if (!member) {
			this.send(`Sorry, you're not allowed to manage roles in this channel.`);
			return;
		}

		if (!action) {
			this.send('Please specify action: `add` or `remove`.');
			return;
		}

		let role = this.params[1];
		if (!role) {
			this.send(`Please specify role to ${action}: \`/role ${action} SomeRole\`.\nList of allowed roles: ${format_options(public_roles)}.`, message_timeout*2);
			return;
		}

		role = role.toLowerCase();

		if (!public_roles.includes(role)) {
			this.send(`List of allowed roles: ${format_options(public_roles)}`);
			return;
		}

		let roleObject = this.findRole(role);
		if (!roleObject) {
			this.send(`Sorry, I can't find such role.`);
			return;
		}

		switch (action) {
		case 'add':
			member.addRole(roleObject).catch(err=>{ console.log(`ERROR: can't add role!`, err); });
			this.send(`Role \`${role}\` added!`);
			break;
		case 'rm':
		case 'remove':
			member.removeRole(roleObject).catch(err=>{ console.log(`ERROR: can't remove role!`, err); });
			this.send(`Role \`${role}\` removed!`);
			break;
		default:
		}
	}
}

class Bot {
	constructor() {}

	handle(msg) {
		if (!this.allowed(msg)) {
			return;
		}

		new Command(msg);
	}

	allowed(msg) {
		let text = msg.content;
		if (!text || !text.startsWith('/')) {
			return false;
		}

		return true;
	}
}

const bot = new Bot();
client.on('message', message => {
	bot.handle(message);
});

client.login(token);

module.exports = {};