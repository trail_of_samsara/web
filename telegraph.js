const config = require('./config.js');
let telegraph = require('telegraph-node');
let ph = new telegraph();

const token = config.telegraph;

let posts = [];
let update_ts;
const cache_ts = 5 * 60 * 1000;

async function update_posts(){
	if (update_ts && (Date.now() - update_ts < cache_ts)) {
		return posts;
	}

	// TODO: fix timeout issue when telegra.ph is not available
	return posts;

	try {
		let {pages} = await ph.getPageList(token, {});
		posts = pages;
		update_ts = Date.now();
	} catch(err){
		console.error(err);
	};

	return posts;
}

module.exports = {
	posts: update_posts
};
