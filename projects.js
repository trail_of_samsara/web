module.exports = [{
	url: 'https://github.com/norlin',
	name: 'GitHub',
	description: 'Web, iOS development and so on'
}, {
	url: 'https://gitlab.com/norlin_ru',
	name: 'GitLab',
	description: 'Game development projects, mostly'
}, {
    url: 'https://norlin.games/blog/',
    name: 'Norlin Games',
    description: 'A blog about gamedev and Knightmare Lands',
}];
