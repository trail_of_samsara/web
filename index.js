const path = require('path');
const sep = path.sep;
const express = require('express');
const less = require('less-middleware');
const app = express();
const api = require('./telegraph.js');
const render = require('./dust.js').render;
const projects = require('./projects.js');

app.use(less(path.join(__dirname, 'styles'), {
	dest: path.join(__dirname, 'public'),
	preprocess: {
		path: function(pathname, req) {
			return pathname.replace(sep+'css'+sep, sep);
		}
	}
}));
app.use(express.static(__dirname + '/public'));

app.get('/', async function(req, res, next){
	let html = await render('index', {projects: projects});
	res.status(200).send(html);
});

app.get('/blog', async function(req, res, next){
	let posts = await api.posts();

	if (!posts)
	{
		let html = await render('404', {msg: 'No posts found!'});
		res.status(404).send(html);
		return;
	}

	let html = await render('blog', {posts: posts});
	res.status(200).send(html);
});

app.get(/^\/(\w+)(\/.*$)|$/, async function(req, res, next){
	try {
		let html = await render(req.params[0]);
		res.status(200).send(html);
		return;
	} catch(e) {
		return next();
	}
});

app.use(async function(req, res, next) {
	let html = await render('404', {msg: 'Page is not found!'});
	res.status(404).send(html);
});

app.listen(3010, 'localhost', function(){
	console.log('App is running!');
});
